namespace PackerProgram.Common;

public enum SortOrderType {NATURAL, SHORT_TO_LONG, LONG_TO_SHORT};