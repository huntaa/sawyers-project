namespace PackerProgram.Common;

public struct ProgramMode {
    public SortOrderType SortOrder;
    public uint MaxPackSize;
    public float MaxPackWeight;
}