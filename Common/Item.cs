namespace PackerProgram.Common;

public struct Item {
    public uint Id;
    public uint LengthMillimetres;
    public float WeightKilograms;
    public uint Count;
}