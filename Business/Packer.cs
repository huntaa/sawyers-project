using PackerProgram.Common;

namespace PackerProgram.Business;

public class Packer {
    /// <summary>
    /// The max weight of each pack
    /// </summary>
    private float _MaxPackWeightKilograms;

    /// <summary>
    /// The max number of items in each pack.
    /// <summary>
    private uint _MaxPackCount;

    /// <summary>
    /// Returns the number of candidate items that will fit in the current pack
    /// </summary>
    private uint _CalcMaxItemQuantity(Pack currentPack, Item candidate) {
        uint maxByCount = Math.Min(
            (uint) (_MaxPackCount - currentPack.ItemCount),
            candidate.Count);
            

        // casting to int rounds down to neareset int
        uint maxByWeight = 
            (uint) ((_MaxPackWeightKilograms - currentPack.WeightKilograms) / candidate.WeightKilograms);

        return Math.Min(maxByCount, maxByWeight);
    }

    public Packer(float maxPackWeight, uint maxPackCount) {
        _MaxPackCount = maxPackCount;
        _MaxPackWeightKilograms = maxPackWeight;
    }

    public List<Pack> GeneratePacks(List<Item> inputData) {
        var result = new List<Pack>();

        var currentPack = new Pack();
        
        foreach(var item in inputData) {
            // Item is value type, therefore item is copied on assignment.
            var inputItem = item;
            uint numToInsert = 0;

            if(item.WeightKilograms > _MaxPackWeightKilograms)
                throw new Exception(string.Format(
                    "Item with ID {0} will not fit into a pack",
                    item.Id));
            
            while(inputItem.Count > 0) {
                numToInsert = _CalcMaxItemQuantity(currentPack, inputItem);

                // set this to be the max length if the item is larger than the current max length
                currentPack.MaxLengthMillimetres = currentPack.MaxLengthMillimetres < item.LengthMillimetres 
                    ? item.LengthMillimetres 
                    : currentPack.MaxLengthMillimetres;

                currentPack.ItemCount += numToInsert;
                currentPack.WeightKilograms += numToInsert * item.WeightKilograms;

                inputItem.Count -= numToInsert;

                if(numToInsert > 0)
                    currentPack.Items.Add(new Item{
                        Id = inputItem.Id,
                        LengthMillimetres = inputItem.LengthMillimetres,
                        WeightKilograms =inputItem.WeightKilograms,
                        Count = numToInsert});

                // if the current pack could not fit the entire item, add this pack to result and start a new pack
                if (inputItem.Count > 0 && numToInsert == 0) {
                    result.Add(currentPack);
                    currentPack = new Pack();
                    var quantityRemaining = inputItem.Count;
                }
            }
        }
        result.Add(currentPack);

        return result;
    }
}