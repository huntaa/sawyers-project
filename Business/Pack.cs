using PackerProgram.Common;

namespace PackerProgram.Business;

public struct Pack {
    public Pack() {
        MaxLengthMillimetres = 0;
        WeightKilograms = 0;
        ItemCount = 0;
        Items = new List<Item>();
    }
    public List<Item> Items;
    public uint MaxLengthMillimetres;
    public float WeightKilograms;
    public uint ItemCount;
}