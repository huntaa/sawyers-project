using PackerProgram.Common;

namespace PackerProgram.Infrastructure;

public static class InputReader {
    public static ProgramMode? GetProgramModeFromStdin()
    {
        string? header = Console.ReadLine();

        if(header == null) {
            Console.WriteLine("Could not read the header as stdin is empty.");
            return null;
        }

        string[] headerFields = _GetHeaderFieldsFromString(header);

        if(headerFields == null) {
            return null;
        }

        if(!Enum.TryParse(headerFields[0], out SortOrderType sortOrder)) {
            Console.WriteLine("The first header field is not a valid sort order.");
            return null;
        }

        if(!uint.TryParse(headerFields[1], out uint maxPackSize)) {
            Console.WriteLine("The second field is not a valid unsigned integer.");
            return null; 
        }

        if(!float.TryParse(headerFields[2], out float maxPackWeight)) {
            Console.WriteLine("The third field is not a valid float");
            return null;
        }


        return new ProgramMode {
            SortOrder = sortOrder,
            MaxPackSize = maxPackSize,
            MaxPackWeight = maxPackWeight};
    }

    public static List<Item> GetInputDataFromStdin(SortOrderType sortOrder) {
        var itemList = new List<Item>();

        uint rowIndex = 1;
        string? row;
        while(true)
        {
            row = Console.ReadLine();
            if(row == null) {
                break;
            }
            
            string[]? fields = _GetDataFieldsFromString(row);
            
            if(fields == null) {
                return null;
            }

            if(!uint.TryParse(fields[0], out uint itemId)) {
                Console.WriteLine(
                    "The first field on line {0} is not a valid unsigned integer", 
                    rowIndex);
                return null;
            }

            if(!uint.TryParse(fields[1], out uint itemLength)) {
                Console.WriteLine(
                    "The second field on line {0} is not a valid unsigned integer",
                    rowIndex
                );
                return null;
            }

            if(!uint.TryParse(fields[2], out uint itemQuantity)) {
                Console.WriteLine(
                    "The third field on line {0} is not a valid unsigned integer",
                    rowIndex
                );
                return null;
            }

            if(!float.TryParse(fields[3], out float pieceWeight)) {
                Console.WriteLine(
                    "The fourth field on line {0} is not a valid flaot",
                    rowIndex
                );
                return null;
            }

            var item = new Item{
                Id = itemId,
                LengthMillimetres = itemLength,
                WeightKilograms = pieceWeight,
                Count = itemQuantity};

            itemList.Add(item);

            rowIndex++;
        }
        var itemCompaererFactory = new ItemCompaererFactory(sortOrder);
        var comparer = itemCompaererFactory.Build();

        // Using List.Sort because it sorts the items in place, and does not copy the list.
        itemList.Sort(comparer);
        return itemList;
    }

    private static string[]? _GetHeaderFieldsFromString(string headerRow) {
        string[] fields = headerRow.Split(",");
        
        if(fields.Count() != 3 ) {
            Console.WriteLine("The header row has the wrong number of fields.");
            return null;
        }

        return fields;
    }

    private static string[]? _GetDataFieldsFromString(string dataRow) {
        string[] fields = dataRow.Split(",");

        if(fields.Count() != 4) {
            Console.WriteLine("Read a data row with the wrong number of fields.");
            return null;
        }

        return fields;
    }
}