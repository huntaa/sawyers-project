using PackerProgram.Common;

namespace PackerProgram.Infrastructure;

public class ItemCompaererFactory {
    private SortOrderType _SortOrder;
    public ItemCompaererFactory(SortOrderType sortOrder) {
        _SortOrder = sortOrder;
    }

    public ItemComparer Build() {
        if(_SortOrder == SortOrderType.SHORT_TO_LONG) {
            return new AscendingItemComparer();
        }
        else if(_SortOrder == SortOrderType.LONG_TO_SHORT) {
            return new DescendingItemComparer();
        }
        else {
            return new NaturalItemComparer();
        }
    }
}