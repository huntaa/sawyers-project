using System;
using System.Collections.Generic;

using PackerProgram.Common;

namespace PackerProgram.Infrastructure;

public abstract class ItemComparer : IComparer<Item> 
{
    public int Compare(Item x, Item y) {
        return SortFunction(x, y);
    }

    protected abstract int SortFunction(Item x, Item y);
}


public class AscendingItemComparer : ItemComparer {
     protected override int SortFunction(Item x, Item y) {
        return (int)x.LengthMillimetres - (int)y.LengthMillimetres;
    }
}


public class DescendingItemComparer : ItemComparer {
    protected override int SortFunction(Item x, Item y)
    {
        return (int)y.LengthMillimetres - (int)x.LengthMillimetres;
    }
}


public class NaturalItemComparer : ItemComparer {
        protected override int SortFunction(Item x, Item y)
    {
        return 0;
    }
}