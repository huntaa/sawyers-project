﻿using PackerProgram.Common;
using PackerProgram.Infrastructure;
using PackerProgram.Business;

var mode = InputReader.GetProgramModeFromStdin();

Console.WriteLine(string.Format("{0}, {1}, {2}", mode.Value.MaxPackSize, mode.Value.MaxPackWeight, mode.Value.SortOrder));

var itemList = InputReader.GetInputDataFromStdin(mode.Value.SortOrder);
var packer = new Packer(mode.Value.MaxPackWeight, mode.Value.MaxPackSize);
var packs = packer.GeneratePacks(itemList);

uint packNumber = 1;
foreach(var pack in packs) {
    Console.WriteLine("Pack Number: " + packNumber);
    foreach(var item in pack.Items) {
        Console.WriteLine(string.Format(
            "{0}, {1}, {2}, {3}",
            item.Id,
            item.LengthMillimetres,
            item.Count,
            item.WeightKilograms));
    }
    Console.WriteLine(string.Format(
        "Pack Length: {0}, Pack Weight: {1}",
        pack.MaxLengthMillimetres,
        pack.WeightKilograms));

    packNumber += 1;
}